﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TileController : MonoBehaviour {
	
	public Vector2 TargetPosition; //position to move to on swipe
	public Vector2 GridPosition; //position in grid
	public float MovementSpeed = 500f;
	public Text Number;

	private RectTransform _rectTransform;

	// Use this for initialization
	void Start () {

		_rectTransform = GetComponent<RectTransform> ();
	}
	
	// Update is called once per frame
	void Update () {

		float step = MovementSpeed * Time.deltaTime;

		_rectTransform.anchoredPosition = Vector3.MoveTowards (this._rectTransform.anchoredPosition, this.TargetPosition, step);
	}

	public void SetGridPosition(Vector2 gridPosition){

		GridPosition.x += gridPosition.x;
		GridPosition.y += gridPosition.y;


		if (GridPosition.x < 0)
			GridPosition.x = 0;
		if (GridPosition.y < 0)
			GridPosition.y = 0;
		if (GridPosition.x > 3)
			GridPosition.x = 3;
		if (GridPosition.y > 3)
			GridPosition.y = 3;

		TargetPosition = GridController.PositionsArray [(int)GridPosition.x, (int)GridPosition.y];
		
	}
}
