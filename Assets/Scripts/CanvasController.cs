﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CanvasController : MonoBehaviour {

	public Text ScoreText;
	public Text HighScore;

	public GameObject MessageBox;
	public Text MessageBoxText;

	public GameObject ResetButton;

	// Use this for initialization
	void Start () {
		GameState.GameCompletedEvent += HandleGameCompletedEvent;

		//should be in a function, but you are out of time :(
		MessageBoxText.text = "Start Game";
		MessageBox.SetActive (true);
		ResetButton.SetActive (false);
	}

	void HandleGameCompletedEvent ()
	{
		print("Completed");
		MessageBoxText.text = "YOU WON";
		MessageBox.SetActive (true);
		ResetButton.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		ScoreText.text = GameState.Instance.Score.ToString ();
		HighScore.text = GameState.Instance.HighScore.ToString ();

	}

	public void OnRestartClicked(){
		MessageBox.SetActive (false);
		GameState.Instance.InitialiseGrid ();
		ResetButton.SetActive (true);
	}

	public void OnQuitClicked(){
		Application.Quit ();
	}
}
