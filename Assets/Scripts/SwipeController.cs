﻿/**
 * Fires events for either swipe or keyboard events
 * Telling where user intends to move
 * 
 * Swipe only works on touch enabled devices
 * User keyboard otherwise
 */

using UnityEngine;
using System.Collections;

public class SwipeController : MonoBehaviour {
	
	public enum DIRECTION{
		UP,
		LEFT,
		DOWN,
		RIGHT
	}

	public bool DisableKeyboardEvents = false;
	
	public delegate void Swipe(DIRECTION direction);
	public static event Swipe SwipeEvent;
	
	public float Sensitivity = 0; //the capacity to avoid small drags, so they can be considered as clicks
	
	private Vector3 _startPosition, _endPosition;
	private bool _isSwipeAllowed;
	
	// Use this for initialization
	void Start () {
		_isSwipeAllowed = true;
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved && _isSwipeAllowed) {
			// Get movement of the finger since last frame
			Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
			PerformTouch(touchDeltaPosition);
			print (touchDeltaPosition);
			_isSwipeAllowed = false;
			// Move object across XY plane
			//			transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * speed, 0);
		}
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
			_isSwipeAllowed = true;
		}

		if (DisableKeyboardEvents)
			return;

		DIRECTION direction;
		if(Input.GetKeyUp(KeyCode.UpArrow)){
			direction = DIRECTION.UP;
			if (SwipeEvent != null)
				SwipeEvent (direction);
		}else if(Input.GetKeyUp(KeyCode.DownArrow)){
			direction = DIRECTION.DOWN;
			if (SwipeEvent != null)
				SwipeEvent (direction);
		}else if(Input.GetKeyUp(KeyCode.LeftArrow)){
			direction = DIRECTION.LEFT;
			if (SwipeEvent != null)
				SwipeEvent (direction);
		}else if(Input.GetKeyUp(KeyCode.RightArrow)){
			direction = DIRECTION.RIGHT;
			if (SwipeEvent != null)
				SwipeEvent (direction);
		}


	}
	
	void PerformTouch(Vector2 deltaTouch){
		
		float x = Mathf.Abs(deltaTouch.x);
		float y = Mathf.Abs(deltaTouch.y);
		
		
		if(x < y){
			
			if( deltaTouch.y > 0){
				if(SwipeEvent != null)
					SwipeEvent(DIRECTION.UP);
				print ("1");
			}else if(deltaTouch.y < 0){
				if(SwipeEvent != null)
					SwipeEvent(DIRECTION.DOWN);
				print ("2");
			}
			
			
		}else{
			
			if( deltaTouch.x > 0){
				if(SwipeEvent != null)
					SwipeEvent(DIRECTION.RIGHT);
				print ("3");
			}else if(deltaTouch.x < 0){
				
				if(SwipeEvent != null)
					SwipeEvent(DIRECTION.LEFT);
				print ("4");
			}
		}
		// start and end positions become equal after first touch
		// so that lifting finger becomes neccessary
		this._startPosition = Input.mousePosition; 
	}
	
	void OnDestroy(){
		SwipeEvent = null;
	}
}