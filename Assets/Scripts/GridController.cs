﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GridController : MonoBehaviour {

	public GameObject Grid;
	public GameObject Tile;
	public static Vector2[,] PositionsArray;
	
	private Dictionary<string,GameObject> tiles;

	GameState _gameState;

	void Awake(){

		SwipeController.SwipeEvent += HandleSwipeEvent;

		PositionsArray = new Vector2[4,4];
		tiles = new Dictionary<string,GameObject> ();
		CalculateTilePositions ();

		_gameState = GameState.Instance;

		GenerateTiles ();
		_gameState.InitialiseGrid ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		UpdateTiles ();

	}

	void CalculateTilePositions(){

		int rowCount = 4;

		GameObject tempTile = Instantiate (Tile);
		RectTransform tileRect = tempTile.GetComponent<RectTransform>();
		RectTransform gridRect = Grid.GetComponent<RectTransform> ();

		//because the grid is square, the horizontal and vertical spacing will be same
		float spacing = gridRect.rect.width - (tileRect.rect.width * rowCount);
		spacing /= 5;

		float x, y = spacing;
		for (int row=0; row<rowCount; row++) {
			x=spacing;
			for(int col=0; col<rowCount; col++) {
				PositionsArray[row,col] = new Vector2(x,-y);
				x += tileRect.rect.width + spacing;
			}
			y += tileRect.rect.height + spacing;
		}

		Destroy (tempTile); //remove the temporary object
	}



	void HandleSwipeEvent (SwipeController.DIRECTION direction)
	{
		bool didMove = false;
		//x shows rows and y shows columns of array
		Vector2 gridDirection = new Vector2(0,0);
		switch (direction) {
		case SwipeController.DIRECTION.UP:
			gridDirection.x = -1;
			didMove = _gameState.MoveGridUp();

			break;
		case SwipeController.DIRECTION.DOWN:
			gridDirection.x = 1;
			didMove = _gameState.MoveGridDown();

			break;
		case SwipeController.DIRECTION.LEFT:
			gridDirection.y = -1;
			didMove = _gameState.MoveGridLeft();

			break;
		case SwipeController.DIRECTION.RIGHT:
			gridDirection.y = 1;
			didMove = _gameState.MoveGridRight();

			break;
		default:
			//do nothing
			break;
		}

		// Generate a random tile only if one or more tiles moved
		if(didMove)
			_gameState.GenerateARandomTile ();


		//foreach (GameObject tile in tiles) {
		//	tile.GetComponent<TileController>().SetGridPosition(gridDirection);
		//}
	}


	//just to test the positions generated
	void GenerateTiles(){
		
		for (int row=0; row<4; row++) {
			
			for(int col=0; col<4; col++) {

				GameObject tile = Instantiate(Tile);
				tile.GetComponent<RectTransform>().anchoredPosition = PositionsArray[row,col];
				tile.transform.SetParent(gameObject.transform,false);
				
				tiles.Add(row.ToString()+col,tile);
				
				TileController tileController = tile.GetComponent<TileController>();
				tileController.TargetPosition = PositionsArray[row,col];
				tileController.GridPosition = new Vector2(row,col);
				tileController.Number.text = _gameState.ValuesGrid[row,col].ToString();

				if(_gameState.ValuesGrid[row,col] == 0){
					tile.SetActive(false);
				}
			}
		}	
		//create first two random tiles
	}

	void UpdateTiles(){

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {

				GameObject tileToSearch;
				tiles.TryGetValue(i.ToString()+j, out tileToSearch);
				if(tileToSearch != null){
					if(_gameState.ValuesGrid[i,j] == 0){
						tileToSearch.SetActive(false);
					}else{
						tileToSearch.SetActive(true);
						tileToSearch.GetComponent<TileController>().Number.text = _gameState.ValuesGrid[i,j].ToString();
					}
				}
			}
		}
	}


}
