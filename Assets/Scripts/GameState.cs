﻿using UnityEngine;
using System.Collections;

public class GameState {

	public static event System.Action GameCompletedEvent;
	public static event System.Action GameLostEvent;

	public int[,] ValuesGrid = new int[4,4];

	private int _score;
	public int Score{
		get{
			return _score;
		}
	}

	public int HighScore{
		get{
			int highScore = PlayerPrefs.GetInt(Constants.HIGH_SCORE, _score);
			if(_score > highScore){
				PlayerPrefs.SetInt(Constants.HIGH_SCORE, _score);
				highScore = _score;
			}
			return highScore;//PlayerPrefs.GetInt(Constants.HIGH_SCORE);
		}
	}

	private int _gridWidth = 3;

	//More chances for and less chances for 4
	private int[] probabilityArray = new int[]{2,2,2,2,2,2,2,2,4,4};

	private static GameState _instance;
	public static GameState Instance {

		get{
			if(_instance == null){
				_instance = new GameState();
			}
			return _instance;
		}
	}

	private GameState(){
//		PlayerPrefs.DeleteAll();
		/*for (int col=0; col<=_gridWidth; col++) {
			for(int row=0; row<=_gridWidth; row++){
				ValuesGrid[row,col]= 0;
			}
		}*/

		InitialiseGrid ();

		if(PlayerPrefs.HasKey(Constants.SCORE_KEY)){
			_score = PlayerPrefs.GetInt(Constants.SCORE_KEY);
		}else{
			PlayerPrefs.SetInt(Constants.SCORE_KEY, 0);
			_score = 0;
		}

		if(!PlayerPrefs.HasKey(Constants.HIGH_SCORE))
			PlayerPrefs.SetInt(Constants.HIGH_SCORE, 0);

	}

	public void InitialiseGrid(){
		for (int col=0; col<=_gridWidth; col++) {
			for(int row=0; row<=_gridWidth; row++){
				ValuesGrid[row,col]= 0;
			}
		}
		GenerateARandomTile ();
		GenerateARandomTile ();
		_score = 0;
	}

	void SaveHighScore(){

		if(_score > HighScore){
			PlayerPrefs.SetInt(Constants.SCORE_KEY, _score);
		}
	}

	public void GenerateARandomTile(){
		Vector2 randomPos;
		int count = 0;
		do {
			randomPos = new Vector2(Random.Range(0,3),Random.Range(0,3));
			count++;
			if(count > 15)
				break;
		} while (ValuesGrid[(int)randomPos.x,(int)randomPos.y] != 0);

		ValuesGrid [(int)randomPos.x, (int)randomPos.y] = probabilityArray [(int)Random.Range (0, 9)];

	}


	//each function in this region returns a bool indicating
	//that either the tiles moved or not
	#region GridControls
	public bool MoveGridDown(){
		bool didMove = false;
		for (int col=0; col<=_gridWidth; col++) {

			for (int row=_gridWidth-1; row>=0; row--) {

				if (ValuesGrid [row, col] == 0)
					continue;

				int currentPointer = row;
				while (currentPointer < _gridWidth) {
					if (ValuesGrid [currentPointer + 1, col] == 0) {
						ValuesGrid [currentPointer + 1, col] = ValuesGrid [currentPointer, col];
						ValuesGrid [currentPointer, col] = 0;
						didMove = true;

					} else if (ValuesGrid [currentPointer + 1, col] == ValuesGrid [currentPointer, col]) {
						ValuesGrid [currentPointer + 1, col] += ValuesGrid [currentPointer, col];
						_score += ValuesGrid [currentPointer + 1, col];
						ValuesGrid [currentPointer, col] = 0;
						didMove = true;
						currentPointer++;
					}
					//Debug.Log(currentPointer+ " " + ValuesGrid[currentPointer, col] + " " + ValuesGrid[currentPointer+1, col]);
					currentPointer++;

				}

			}
		}
		NotifyIfGameComplete();
		return didMove;
	}

	public bool MoveGridUp(){
		bool didMove = false;
		for (int col=0; col<=_gridWidth; col++) {
			
			for (int row=1; row<=_gridWidth; row++) {
				
				if (ValuesGrid [row, col] == 0)
					continue;
				
				int currentPointer = row;
				while (currentPointer > 0) {
					if (ValuesGrid [currentPointer - 1, col] == 0) {
						ValuesGrid [currentPointer - 1, col] = ValuesGrid [currentPointer, col];
						ValuesGrid [currentPointer, col] = 0;
						didMove = true;
						
					} else if (ValuesGrid [currentPointer - 1, col] == ValuesGrid [currentPointer, col]) {
						ValuesGrid [currentPointer - 1, col] += ValuesGrid [currentPointer, col];
						_score += ValuesGrid [currentPointer - 1, col];
						ValuesGrid [currentPointer, col] = 0;
						didMove = true;
						currentPointer--;
					}
					//Debug.Log(currentPointer+ " " + ValuesGrid[currentPointer, col] + " " + ValuesGrid[currentPointer+1, col]);
					currentPointer--;
					
				}
			}
		}
		NotifyIfGameComplete();
		return didMove;
	}

	public bool MoveGridLeft(){

		bool didMove = false;
		for (int row=0; row<=_gridWidth; row++) {
			
			for (int col=1; col<=_gridWidth; col++) {
				
				if (ValuesGrid [row, col] == 0)
					continue;
				
				int currentPointer = col;
				while (currentPointer > 0) {
					if (ValuesGrid [row, currentPointer - 1] == 0) {
						ValuesGrid [row, currentPointer - 1] = ValuesGrid [row, currentPointer];
						ValuesGrid [row, currentPointer] = 0;
						didMove = true;
						
					} else if (ValuesGrid [row, currentPointer - 1] == ValuesGrid [row, currentPointer]) {
						ValuesGrid [row, currentPointer - 1] += ValuesGrid [row, currentPointer];
						_score += ValuesGrid [row, currentPointer - 1];
						ValuesGrid [row, currentPointer] = 0;
						didMove = true;
						currentPointer--;
					}
					//Debug.Log(currentPointer+ " " + ValuesGrid[currentPointer, col] + " " + ValuesGrid[currentPointer+1, col]);
					currentPointer--;
					
				}
			}
		}
		NotifyIfGameComplete();
		return didMove;
	}

	public bool MoveGridRight(){
		bool didMove = false;
		for (int row=0; row<=_gridWidth; row++) {
			
			for (int col=_gridWidth; col>=0; col--) {
				
				if (ValuesGrid [row, col] == 0)
					continue;
				
				int currentPointer = col;
				while (currentPointer < _gridWidth) {
					if (ValuesGrid [row, currentPointer + 1] == 0) {
						ValuesGrid [row, currentPointer + 1] = ValuesGrid [row, currentPointer];
						ValuesGrid [row, currentPointer] = 0;
						didMove = true;
						
					} else if (ValuesGrid [row, currentPointer + 1] == ValuesGrid [row, currentPointer]) {
						ValuesGrid [row, currentPointer + 1] += ValuesGrid [row, currentPointer];
						_score += ValuesGrid [row, currentPointer + 1];
						ValuesGrid [row, currentPointer] = 0;
						didMove = true;
						currentPointer++;
					}
					//Debug.Log(currentPointer+ " " + ValuesGrid[currentPointer, col] + " " + ValuesGrid[currentPointer+1, col]);
					currentPointer++;
					
				}
			}
		}
		NotifyIfGameComplete();
		return didMove;
	}
	#endregion

	private bool NotifyIfGameLost(){
		for (int row = 0; row <=_gridWidth; row++) {
			for (int col = 0; col <= _gridWidth; col++) {
				if(ValuesGrid[row,col] == 0){
				}

			}
		}
		return false;
	}

	private bool NotifyIfGameComplete(){

		for (int row = 0; row <= _gridWidth; row++) {
			for (int col = 0; col <= _gridWidth; col++) {
				if(ValuesGrid[row,col] == Constants.WINNING_TILE_NUMBER){
					if(GameCompletedEvent != null)
						GameCompletedEvent();
					return true;
				}
			}
		}
		return false;
	}

	public void PrintValuesGrid(){
		for (int col=0; col<=_gridWidth; col++) {
			string temp = "";
			for(int row=0; row<=_gridWidth; row++){
				temp += (ValuesGrid[col,row] + " ");
			}
			Debug.Log (temp);
		}
	}
	
}
