﻿using UnityEngine;
using System.Collections;

public class Constants {
	public static readonly int WINNING_TILE_NUMBER = 2048;
	public static readonly string SCORE_KEY = "Score";
	public static readonly string HIGH_SCORE = "HiScore";
}
